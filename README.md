# Managing Settings

Most configurations are managed in settings.py, and overridden by
settings_local.py, which is typically mounted in a Kubernetes cluster as a
[ConfigMap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/).

# Twilio

Each environment requires a distinct phone number on Twilio, as replies to text
messages and phone calls are routed to the respective environment.

Here's what production settings look like:
```
TWILIO_ACCOUNT_SID = 'AC...'
TWILIO_AUTH_TOKEN = '...'
TWILIO_SMS_VERIFICATION_NUMBER = '+1443.......'
```

# Google Cloud Storage

In order to upload files to GCS, the python library requires that a GCP service
account with a downloaded credentials file.

Here's what the production settings look like:
```
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
GS_BUCKET_NAME = 'mycareerprofile-prod'
GS_PROJECT_ID = 'nth-rarity-194720'
GS_BUCKET_NAME = 'mycareerprofile-prod'
GS_CREDENTIALS = service_account.Credentials.from_service_account_file('/etc/storage-config/key.json')
GS_AUTO_CREATE_BUCKET = True
```

`/etc/storage-config/key.json` is mounted via a [Secret](https://kubernetes.io/docs/concepts/configuration/secret/).


## Ensuring buckets are public by default

Need to ensure that the default permissions on GCS buckets are set to readable
by the public.

```
gsutil defacl ch -u AllUsers:R gs://mycareerprofile-development

```

## Ensuring that buckets allow Cross Origin requests

```
gsutil cors set gs-cors-dev.json gs://mycareerprofile-development

```