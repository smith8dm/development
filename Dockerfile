FROM ubuntu:xenial


RUN apt-get update && apt-get install -y --no-install-recommends \
        apache2 \
        apache2-dev \
        build-essential \
        libapache2-mod-wsgi-py3 \
        libmysqlclient-dev \
        python3 \
        python3-dev \
        python3-pip \
        poppler-utils \
        vim \
    && rm -rf /var/lib/apt/lists/*
RUN pip3 install --upgrade pip==10.0.1 setuptools==38.5.1
ADD requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt
RUN rm /var/log/apache2/*.log && \
    ln -s /dev/stdout /var/log/apache2/access.log && \
    ln -s /dev/stderr /var/log/apache2/error.log

ADD apache.conf /etc/apache2/sites-available/000-default.conf
ADD mycareerprofile /mycareerprofile

CMD ["apache2ctl", "-DFOREGROUND"]