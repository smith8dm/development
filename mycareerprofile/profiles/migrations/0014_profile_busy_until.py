# Generated by Django 2.0 on 2018-02-16 02:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0013_auto_20180216_0251'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='busy_until',
            field=models.DateField(blank=True, null=True),
        ),
    ]
