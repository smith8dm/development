# Generated by Django 2.0 on 2018-05-19 02:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0046_profile_is_willing_to_relocate_for_work'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profileskill',
            name='years_of_experience',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Less than 1 year of experience'), (1, '1 year of experience'), (2, '2 years of experience'), (3, '3 years of experience'), (4, '4 years of experience'), (5, '5 years of experience'), (6, '6 years of experience'), (7, '7 years of experience'), (8, '8 years of experience'), (9, '9 years of experience'), (10, '10 years of experience'), (11, '11 years of experience'), (12, '12 years of experience'), (13, '13 years of experience'), (14, '14 years of experience'), (15, '15 years of experience'), (16, '16 years of experience'), (17, '17 years of experience'), (18, '18 years of experience'), (19, '19 years of experience'), (20, '20 years of experience'), (99, 'More than 20 years of experience')], default=0),
        ),
    ]
