# Generated by Django 2.0 on 2018-07-24 05:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0053_profilepasswordreset'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='profile_type',
            field=models.CharField(choices=[('candidate', 'I am looking to create an employee profile'), ('prospect', 'I am looking for employees')], default='candidate', max_length=20),
        ),
    ]
