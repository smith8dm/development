from django import template
from django.urls import resolve
from profiles.utils import get_previous_manager_page as get_previous_manager_page_util
from profiles.utils import get_next_manager_page as get_next_manager_page_util

register = template.Library()


def filter_resource_by_type(resources, resource_type):
    return [r for r in resources if r.resource_type == resource_type]


def populate_context(context, resource_type, resource_type_display):
    return {
        'resource_type': resource_type,
        'resource_type_display': resource_type_display,
        'resources': filter_resource_by_type(context['resources'], resource_type),
    }


@register.simple_tag(takes_context=True)
def get_previous_manager_page(context):
    return get_previous_manager_page_util(resolve(context['request'].path_info).url_name)

@register.simple_tag(takes_context=True)
def get_next_manager_page(context):
    return get_next_manager_page_util(resolve(context['request'].path_info).url_name)


@register.inclusion_tag('partials/view-profile/photo-resources.html', takes_context=True)
def photo_resources(context, resource_type, resource_type_display):
    return populate_context(context, resource_type, resource_type_display)


@register.inclusion_tag('partials/view-profile/resource-link.html', takes_context=True)
def resource_link(context, resource_type, resource_type_display):
    return populate_context(context, resource_type, resource_type_display)


@register.inclusion_tag('partials/view-profile/video-resources.html', takes_context=True)
def video_resources(context, resource_type, resource_type_display):
    return populate_context(context, resource_type, resource_type_display)

@register.simple_tag
def profile_skill_years_of_experience(profile, skill):
    try:
        return profile.profileskill_set.get(skill=skill).years_of_experience
    except profile.profileskill_set.model.DoesNotExist:
        return 0