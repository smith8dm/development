from django.test import TestCase
from profiles.models import Career
from profiles.models import Profession
from profiles.models import Profile
from profiles.models import Skill


class CareerTreeTestCase(TestCase):
	def test_get_tree_returns_empty_without_professions_and_skills(self):
		Career.objects.create(name='career')

		self.assertEqual(Career.get_tree(), {})

	def test_get_tree_returns_empty_without_skills(self):
		career = Career.objects.create(name='career')
		profession = career.profession_set.create(name='profession')

		self.assertEqual(Career.get_tree(), {})

	def test_get_tree_returns_single_skill(self):
		career = Career.objects.create(name='career')
		profession = career.profession_set.create(name='profession')
		skill = career.skill_set.create(profession=profession, name='skill')

		self.assertEqual(Career.get_tree(), {
			career: {
				profession: [skill],
			},
		})

	def test_get_tree_returns_multiple_skills(self):
		career = Career.objects.create(name='career')
		profession = career.profession_set.create(name='profession')
		skills = [
			career.skill_set.create(profession=profession, name='skill-1'),
			career.skill_set.create(profession=profession, name='skill-2')
		]

		self.assertEqual(Career.get_tree(), {
			career: {
				profession: skills,
			},
		})

	def test_get_tree_returns_multiple_professions(self):
		career = Career.objects.create(name='career')

		profession_1 = career.profession_set.create(name='profession-1')
		profession_1_skill = career.skill_set.create(profession=profession_1, name='skill-1')

		profession_2 = career.profession_set.create(name='profession-2')
		profession_2_skill = career.skill_set.create(profession=profession_2, name='skill-2')

		career_tree = Career.get_tree()

		self.assertEqual(len(career_tree), 1)
		self.assertIn(career, career_tree)

		profession_tree = career_tree[career]

		self.assertIn(profession_1, profession_tree)
		self.assertIn(profession_2, profession_tree)

		self.assertEqual(profession_tree[profession_1], [profession_1_skill])
		self.assertEqual(profession_tree[profession_2], [profession_2_skill])

	def test_get_tree_returns_multiple_careers(self):
		career_1 = Career.objects.create(name='career-1')
		career_1_profession = career_1.profession_set.create(name='career-1-profession')
		career_1_profession_skill = career_1.skill_set.create(profession=career_1_profession, name='skill')

		career_2 = Career.objects.create(name='career-2')
		career_2_profession = career_2.profession_set.create(name='career-2-profession')
		career_2_profession_skill = career_2.skill_set.create(profession=career_2_profession, name='skill')

		career_tree = Career.get_tree()

		self.assertEqual(len(career_tree), 2)
		self.assertIn(career_1, career_tree)
		self.assertIn(career_2, career_tree)

		self.assertIn(career_1_profession, career_tree[career_1])
		self.assertIn(career_2_profession, career_tree[career_2])

		self.assertEqual(career_tree[career_1][career_1_profession], [career_1_profession_skill])
		self.assertEqual(career_tree[career_2][career_2_profession], [career_2_profession_skill])


class ProfileSkillIdsTestCase(TestCase):
	def test_empty_skill_ids(self):
		profile = Profile.objects.create()
		self.assertQuerysetEqual(profile.skill_ids, [])

	def test_single_skill_ids(self):
		career = Career.objects.create()
		profession = career.profession_set.create(career=career)
		skill = Skill.objects.create(career=career, profession=profession)

		profile = Profile.objects.create()
		profile.profileskill_set.create(skill=skill)

		self.assertQuerysetEqual(profile.skill_ids, map(str, [skill.id]))

	def test_multiple_skill_ids(self):
		career = Career.objects.create()
		profession = career.profession_set.create(career=career)
		skill_1 = Skill.objects.create(career=career, profession=profession)
		skill_2 = Skill.objects.create(career=career, profession=profession)

		profile = Profile.objects.create()
		profile.profileskill_set.create(skill=skill_1)
		profile.profileskill_set.create(skill=skill_2)

		self.assertQuerysetEqual(profile.skill_ids, map(str, [skill_1.id, skill_2.id]))


class ProfileCareerTreeTestCase(TestCase):
	def setUp(self):
		self.profile = Profile.objects.create()

	def test_empty_career_tree(self):
		self.assertEqual(self.profile.get_career_tree(), {})

	def test_single_skill_career_tree(self):
		career = Career.objects.create()
		profession = career.profession_set.create()
		skill = profession.skill_set.create(career=career, profession=profession)

		profile_skill = self.profile.profileskill_set.create(skill=skill)

		self.assertEqual(self.profile.get_career_tree(), {
			career: {
				profession: [profile_skill]
			}
		})

	def test_multiple_skill_career_tree(self):
		career_1 = Career.objects.create()
		profession_1 = career_1.profession_set.create()
		skill_1 = career_1.skill_set.create(profession=profession_1)
		profile_skill_1 = self.profile.profileskill_set.create(skill=skill_1)

		career_2 = Career.objects.create()
		profession_2 = career_2.profession_set.create()
		skill_2 = career_2.skill_set.create(profession=profession_2)
		profile_skill_2 = self.profile.profileskill_set.create(skill=skill_2)


		self.assertEqual(self.profile.get_career_tree(), {
			career_1: {
				profession_1: [profile_skill_1]
			},
			career_2: {
				profession_2: [profile_skill_2]
			}
		})