from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.db import models
from django.forms import CheckboxSelectMultiple
from .models import Career
from .models import Profession
from .models import Profile
from .models import ProfileSkill
from .models import ProfileVerification
from .models import Resource
from .models import ResourceUploadManager
from .models import Skill
from .models import Testimonial


class ResourceUploadManagerInline(admin.TabularInline):
    model = ResourceUploadManager
    fields = ('resource_type', 'file', )
    extra = 3


class ResourceInline(admin.TabularInline):
    model = Resource
    fields = ('sort_order', 'resource_type', 'file', )
    readonly_fields = ('resource_type', 'file', )

    def has_add_permission(self, request, obj=None):
        return False


class ProfileSkillInline(admin.TabularInline):
    model = ProfileSkill


class ProfileVerificationInline(admin.TabularInline):
    model = ProfileVerification
    extra = 1

    readonly_fields = (
        'token',
        'is_verified',
        'date_verified',
    )


class TestimonialInline(admin.TabularInline):
    model = Testimonial
    extra = 1


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    classes = ('collapse open',)
    inline_classes = ('collapse open',)

    list_display = (
        'share_id',
        'first_name',
        'last_name',
        'profile_type',
        'email',
        'is_reviewed',
        'is_active',
    )

    search_fields = [
        'id',
        'share_id',
        'first_name',
        'last_name',
        'email',
    ]

    list_filter = (
        ('profile_type', admin.ChoicesFieldListFilter),
        ('is_reviewed', admin.BooleanFieldListFilter),
        ('is_active', admin.BooleanFieldListFilter),
    )

    fields = (
        'user',
        ('first_name', 'last_name', ),
        ('cell_phone_number', 'cell_phone_number_verified', ),
        ('email', 'email_verified', ),
        'secondary_phone_number',
        'profile_type',
        'bio',
        'county',
        'state',
        'postal_code',
        'distance_willing_to_travel',
        'is_available_for_work',
        'is_willing_to_relocate_for_work',
        'is_reviewed',
        'is_active',
        'finished_profile_setup',
        'is_us_citizen',
        'is_authorized_to_work_in_us',
        'is_18_or_older',
        'accepts_terms_of_service',
    )

    readonly_fields = (
        'is_us_citizen',
        'is_authorized_to_work_in_us',
        'is_18_or_older',
        'accepts_terms_of_service',
    )

    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple()},
    }

    inlines = [
        ProfileSkillInline,
        ResourceUploadManagerInline,
        ResourceInline,
        ProfileVerificationInline,
        TestimonialInline,
    ]

    def save_formset(self, request, form, formset, change):
        instances = formset.save()

        if formset.model == ResourceUploadManager:
            for instance in instances:
                instance.upload_resources()

class ProfileInline(admin.StackedInline):
    model = Profile
    extra = 0
    max_num = 0
    show_change_link = True
    verbose_name_plural = 'Profile'

    classes = ('collapse open',)
    inline_classes = ('collapse open',)

    fields = (
        ('first_name', 'last_name', ),
        ('cell_phone_number', 'email', ),
        'bio',
        'is_reviewed',
    )

    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple()},
    }


admin.site.unregister(User)
@admin.register(User)
class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline, )


class ProfessionInline(admin.TabularInline):
    model = Profession


class SkillInline(admin.TabularInline):
    model = Skill


@admin.register(Career)
class CareerAdmin(admin.ModelAdmin):
    inlines = (ProfessionInline, SkillInline, )
