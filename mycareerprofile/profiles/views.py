from django.core.files.base import ContentFile
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import get_user
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import connection
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.shortcuts import HttpResponse
from io import BytesIO
from PIL import Image
from PIL import ImageEnhance
from profiles.forms import ProfileAboutMeForm
from profiles.forms import ProfileForgotPasswordForm
from profiles.forms import ProfileSearchForm
from profiles.forms import ProfileSignupForm
from profiles.forms import ProfileSkillForm
from profiles.forms import ProfilePasswordResetForm
from profiles.forms import ProfilePersonalInfoForm
from profiles.forms import ProfileWorkStatusForm
from profiles.forms import UserSignupForm
from profiles.models import Career
from profiles.models import Profession
from profiles.models import Profile
from profiles.models import ProfileSkill
from profiles.models import ProfileVerification
from profiles.models import ProfilePasswordReset
from profiles.models import PostalCode
from profiles.models import Resource
from profiles.models import ResourceUploadManager
from profiles.models import Skill
from profiles.models import YEARS_OF_EXPERIENCE_CHOICES
from profiles.utils import determine_subsequent_profile_manager_page_for_request
from site_settings.models import Site
from twilio.twiml.messaging_response import Message, MessagingResponse

import json
import os
import requests
import twilio.rest

@login_required
def verification(request, token):
    profile = request.user.profile_set.all()[0]

    verifications = profile.profileverification_set.filter(
        token=token,
        is_verified=False)

    if not verifications:
        return redirect('manage-profile-personal-info')

    for verification in verifications:
        verification.mark_verified()

    messages.add_message(request, messages.SUCCESS, 'Your profile has been verified successfully.')

    return redirect('manage-profile-personal-info')


@login_required
def list(request):
    form = ProfileSearchForm(request.GET if 'csrfmiddlewaretoken' in request.GET else None)

    context = {
        'form': form
    }

    profiles = Profile.objects.filter(is_reviewed=True, is_active=True)

    if form.is_valid():
        if form.cleaned_data['share_id']:
            profiles = profiles.filter(
                share_id=form.cleaned_data['share_id']
            )

        if form.cleaned_data['first_name']:
            profiles = profiles.filter(
                first_name=form.cleaned_data['first_name']
            )

        if form.cleaned_data['jobsite_postal_code']:
            # Find all known postal codes within 50 miles
            postal_code_with_distances = PostalCode.find_within_miles(form.cleaned_data['jobsite_postal_code'], 50)

            location_query = Q()

            for postal_code, distance_in_miles in postal_code_with_distances:
                # Only return profiles with postal codes within travelable distance
                location_query |= Q(
                    postal_code=postal_code,
                    distance_willing_to_travel__gte=distance_in_miles
                )

            profiles = profiles.filter(location_query)

        if form.cleaned_data['professions']:
            profiles = profiles.filter(
                profileskill__skill__profession__in=form.cleaned_data['professions']
            )

            context['selected_professions'] = form.cleaned_data['professions']

        if form.cleaned_data['skills']:
            profiles = profiles.filter(
                profileskill__skill__in=form.cleaned_data['skills']
            )

            context['selected_skills'] = form.cleaned_data['skills']

    profiles = profiles.distinct()

    profiles_by_id = dict([(profile.id, profile) for profile in profiles])

    for resource in Resource.objects.filter(profile__in=profiles, resource_type='profile_photo'):
        profile = profiles_by_id[resource.profile_id]
        if not hasattr(profile, 'photos'):
            profile.photos = []

        profile.photos.append(resource)

    context['profiles'] = profiles

    return render(request, 'list.html', context)


@login_required(login_url='/profiles/register/prospect')
def search(request):
    return render(request, 'search.html', {
        'form': ProfileSearchForm(),
        'careers': Career.objects.all(),
        'professions_json': json.dumps([dict(p) for p in Profession.objects.all().values('id', 'career_id', 'name')]),
        'skills_json': json.dumps([dict(p) for p in Skill.objects.all().values('id', 'profession_id', 'name')]),
    })

def upload_resources_for_profile(profile, files):
    for resource_type, _ in Resource._meta.get_field('resource_type').choices:
        for file in files.getlist(resource_type):
            resource_upload_manager = ResourceUploadManager.objects.create(
                profile=profile,
                resource_type=resource_type,
                file=file
            )
            resource_upload_manager.upload_resources()

def manage_profile(request):
    return redirect('manage-profile-personal-info')

@login_required(login_url='/profiles/register/candidate')
def manage_profile_about_me(request):
    profile = request.user.profile_set.all()[0]

    if request.method == 'POST':
        profile_about_me_form = ProfileAboutMeForm(request.POST, instance=profile)

        if profile_about_me_form.is_valid():
            if profile_about_me_form.has_changed():
                profile_about_me_form.save()

                messages.add_message(request, messages.SUCCESS, 'Your profile has been updated successfully.')

            return redirect(determine_subsequent_profile_manager_page_for_request(request))
    else:
        profile_about_me_form = ProfileAboutMeForm(instance=profile)

    return render(request, 'manage/about-me.html', {
        'profile': profile,
        'resources': profile.resource_set.all().order_by('sort_order'),
        'profile_about_me_form': profile_about_me_form,
    })

@login_required
def manage_profile_personal_info(request):
    profile = request.user.profile_set.all()[0]

    if request.method == 'POST':
        personal_info_form = ProfilePersonalInfoForm(request.POST, instance=profile)

        if personal_info_form.is_valid():
            if personal_info_form.has_changed():
                profile = personal_info_form.save()

                messages.add_message(request, messages.SUCCESS, 'Your profile has been updated successfully.')

            should_proceed = True

            # Check fields that need manual validation before proceeding.
            if not profile.email_verified:
                should_proceed = False

            if not profile.cell_phone_number_verified:
                should_proceed = False

            if should_proceed:
                return redirect(determine_subsequent_profile_manager_page_for_request(request))

            return redirect('manage-profile-personal-info')
    else:
        personal_info_form = ProfilePersonalInfoForm(instance=profile)

    return render(request, 'manage/personal-info.html', {
        'profile': profile,
        'personal_info_form': personal_info_form
    })

@login_required
def manage_profile_work_status(request):
    profile = request.user.profile_set.all()[0]

    if request.method == 'POST':
        work_status_form = ProfileWorkStatusForm(request.POST, instance=profile)

        if work_status_form.is_valid():
            if work_status_form.has_changed():
                profile = work_status_form.save()

                messages.add_message(request, messages.SUCCESS, 'Your profile has been updated successfully.')

            # Only mark as finished if they're going forward.
            if not request.POST.get('go_back'):
                profile.finished_profile_setup = True
                profile.save()

            return redirect(determine_subsequent_profile_manager_page_for_request(request))
    else:
        work_status_form = ProfileWorkStatusForm(instance=profile)

    return render(request, 'manage/work-status.html', {
        'profile': profile,
        'work_status_form': work_status_form
    })

@login_required
def manage_profile_edit_resource(request, resource_id):
    profile = request.user.profile_set.all()[0]
    resource = get_object_or_404(Resource, id=resource_id, profile=profile)

    redirect_url = request.POST.get('redirect_url')
    if not redirect_url:
        redirect_url = request.META.get('HTTP_REFERER')

    if request.method == 'POST':
        if request.POST.get('restore_original_file') == 'true':
            resource.file = resource.original_file

        img = Image.open(BytesIO(resource.file.read()))
        original_format = img.format

        any_visual_changes = False

        # Crop the image
        if request.POST.get('cropper_x'):
            any_visual_changes = True
            img = img.crop((
                float(request.POST['cropper_x']),
                float(request.POST['cropper_y']),
                float(request.POST['cropper_x']) + float(request.POST['cropper_width']),
                float(request.POST['cropper_y']) + float(request.POST['cropper_height'])
            ))

        # Rotate the image
        if request.POST.get('cropper_rotate'):
            any_visual_changes = True
            img = img.rotate(float(request.POST['cropper_rotate']) * -1)

        # Brighten the image
        if request.POST.get('enhancer_brightness'):
            any_visual_changes = True
            enhancer = ImageEnhance.Brightness(img)
            img = enhancer.enhance(float(request.POST.get('enhancer_brightness')))

        if any_visual_changes:
            img_io = BytesIO()
            img.save(img_io, format=original_format)

            resource.file.save(os.path.basename(resource.file.name), ContentFile(img_io.getvalue()), save=False)

        resource.save()

        return redirect(redirect_url)

    return render(request, 'manage/edit-resource.html', {
        'profile': profile,
        'resource': resource,
        'redirect_url': redirect_url,
    })

@login_required
def manage_profile_resource(request, resource_type, resource_type_display):
    profile = request.user.profile_set.all()[0]

    if request.method == 'POST':
        # Upload and redirect to the current URL when uploading files.
        if request.FILES:
            upload_resources_for_profile(profile, request.FILES)

            messages.add_message(request, messages.SUCCESS, 'File has been uploaded to your profile.')

            return redirect(request.path)

        return redirect(determine_subsequent_profile_manager_page_for_request(request))

    return render(request, 'manage/resource.html', {
        'profile': profile,
        'resources': profile.resource_set.filter(resource_type=resource_type).order_by('sort_order'),
        'resource_type': resource_type,
        'resource_type_display': resource_type_display,
    })

def manage_profile_conversation_videos(request):
    return manage_profile_resource(request, 'conversation_video', 'Video Conversations')

def manage_profile_certifications(request):
    return manage_profile_resource(request, 'certification_photo', 'Licenses/Certifications')

def manage_profile_education(request):
    return manage_profile_resource(request, 'education_photo', 'Education')

def manage_profile_letters_of_recommendation(request):
    return manage_profile_resource(request, 'letter_of_recommendation_photo', 'Letters of Recommendation')

def manage_profile_tool_photos(request):
    return manage_profile_resource(request, 'tool_photo', 'Tool Photos')

def manage_profile_profile_photos(request):
    return manage_profile_resource(request, 'profile_photo', 'Profile photos')

def manage_profile_resume(request):
    return manage_profile_resource(request, 'resume_photo', 'Resume')

def manage_profile_skill_assessments(request):
    return manage_profile_resource(request, 'skill_assessment_photo', 'Skill Assessments')

def manage_profile_career_photos(request):
    return manage_profile_resource(request, 'career_photo', 'Career Photos')

def manage_profile_work_videos(request):
    return manage_profile_resource(request, 'work_video', 'Project/Work Videos')

@login_required
def manage_profile_add_skills(request):
    profile = request.user.profile_set.all()[0]

    if request.method == 'POST':
        added_skills = False

        for field_name, value in request.POST.items():
            if not field_name.startswith('skill_id.'):
                continue

            skill_id = value

            years_of_experience = request.POST.get('years_of_experience.%s' % (skill_id, ))

            try:
                profile_skill = profile.profileskill_set.get(skill__id=skill_id)
            except:
                profile_skill = None

            profile_skill_form = ProfileSkillForm({
                'skill': skill_id,
                'years_of_experience': years_of_experience,
            }, instance=profile_skill)

            profile_skill_form.instance.profile = profile
            profile_skill_form.save()

            added_skills = True

        if not profile.profileskill_set.count():
            messages.add_message(request, messages.WARNING, 'Please add at least one skill to your profile')
            return redirect('manage-profile-add-skills')

        if added_skills:
            messages.add_message(request, messages.SUCCESS, 'Profile skill(s) has been added to your profile.')

        if request.POST.get('additional_skills') == 'yes':
            return redirect('manage-profile-add-skills')

        return redirect(determine_subsequent_profile_manager_page_for_request(request))

    else:
        profile_skill_form = ProfileSkillForm()

    return render(request, 'manage/add-skills.html', {
        'profile': profile,
        'careers': Career.objects.all(),
        'professions_json': json.dumps([dict(p) for p in Profession.objects.all().values('id', 'career_id', 'name')]),
        'skills_json': json.dumps([dict(p) for p in Skill.objects.all().values('id', 'profession_id', 'name')]),
        'years_of_experience_choices': YEARS_OF_EXPERIENCE_CHOICES,
        'profile_skill_form': profile_skill_form
    })

@login_required
def manage_profile_skills(request):
    profile = request.user.profile_set.all()[0]

    return render(request, 'manage/skills.html', {
        'profile': profile,
        'profile_skills': profile.profileskill_set.all()
    })


@login_required
def manage_profile_complete(request):
    profile = request.user.profile_set.all()[0]

    return render(request, 'manage/complete.html', {
        'profile': profile
    })


@login_required
def manage_profile_remove_resource(request, resource_id):
    profile = request.user.profile_set.all()[0]
    profile.resource_set.filter(id=resource_id).delete()

    profile.is_reviewed = False
    profile.save(update_fields=['is_reviewed'])

    messages.add_message(request, messages.SUCCESS, 'Resource has been removed from your profile.')

    redirect_url = request.META.get('HTTP_REFERER')
    if not redirect_url:
        redirect_url = 'manage-profile-about-me'

    return redirect(redirect_url)


@login_required
def manage_profile_remove_skill(request, profile_skill_id):
    profile = request.user.profile_set.all()[0]
    profile.profileskill_set.filter(id=profile_skill_id).delete()

    profile.is_reviewed = False
    profile.save(update_fields=['is_reviewed'])

    messages.add_message(request, messages.SUCCESS, 'Skill has been removed from your profile.')

    return redirect('manage-profile-skills')


def signin(request):
    if request.user.is_authenticated:
        return redirect('manage-profile')

    if request.method == 'POST':
        user = authenticate(
            username=request.POST.get('username'),
            password=request.POST.get('password')
        )

        if user:
            login(request, user)

            try:
                profile = request.user.profile_set.all()[0]
            except:
                profile = Profile()

            if profile.profile_type == 'candidate':
                return redirect('manage-profile')

            return redirect('profiles-search')
        else:
            messages.add_message(request, messages.WARNING, 'Invalid username / password provided.')

    return render(request, 'signin.html')


def forgot_password(request):
    if request.method == 'POST':
        form = ProfileForgotPasswordForm(request.POST)

        if form.is_valid():
            for profile in form.profiles:
                profile.trigger_password_reset()

            return redirect('profile-forgot-password-sent')

    else:
        form = ProfileForgotPasswordForm()

    return render(request, 'forgot-password.html', {
        'form': form
    })


def forgot_password_sent(request):
    return render(request, 'forgot-password-sent.html')


def reset_password(request, token):
    logout(request)

    password_reset = get_object_or_404(ProfilePasswordReset, token=token)
    user = password_reset.profile.user

    if request.method == 'POST':
        form = ProfilePasswordResetForm(request.POST)

        if form.is_valid():
            user.set_password(form.cleaned_data['password'])
            user.save()

            messages.add_message(request, messages.SUCCESS, 'Your password has been reset. Please log in with your new password.')

            return redirect('profile-signin')

    else:
        form = ProfilePasswordResetForm()

    return render(request, 'reset-password.html', {
        'form': form,
        'token': password_reset.token
    })


def register(request, profile_type=None):
    if not profile_type:
        profile_type = 'candidate'

    initial = {
        'profile_type': profile_type,
    }

    if request.user.is_authenticated:
        return redirect('manage-profile')

    if request.method == 'POST':
        user_signup_form = UserSignupForm(request.POST)
        profile_signup_form = ProfileSignupForm(request.POST, initial=initial)

        if user_signup_form.is_valid() and profile_signup_form.is_valid():
            user = user_signup_form.save()

            profile_signup_form.instance.user = user
            profile_signup_form.save()

            login(request, user)

            if profile_type == 'candidate':
                return redirect('manage-profile')

            return redirect('profiles-search')
    else:
        user_signup_form = UserSignupForm()
        profile_signup_form = ProfileSignupForm(initial=initial)

    return render(request, 'register.html', {
        'user_signup_form': user_signup_form,
        'profile_signup_form': profile_signup_form,
        'profile_type': profile_type,
    })

def signout(request):
    logout(request)

    messages.add_message(request, messages.SUCCESS, 'You have been signed out successfully.')

    return redirect('profile-signin')

def view_profile(request, profile_id):
    profile = get_object_or_404(Profile, id=profile_id)

    user = get_user(request)
    if user.is_authenticated:
        authenticated_profile = user.profile_set.all()[0]
    else:
        authenticated_profile = None

    resources = profile.resource_set.all().order_by('sort_order')

    context = {
        'profile': profile,
        'authenticated_profile': authenticated_profile,
        'resources': resources,
        'testimonials': profile.testimonial_set.all(),
        'profile_photos': [r for r in resources if r.resource_type == 'profile_photo'],
    }

    return render(request, 'view-profile.html', context)

def twilio_sms_callback(request):
    verifications = ProfileVerification.objects.filter(
        target_field='cell_phone_number_verified',
        send_to=request.POST.get('From'),
        is_verified=False)

    for verification in verifications:
        verification.mark_verified()

    resp = MessagingResponse()
    if verifications.count():
        resp.message('Your phone number has been verified.')
    else:
        resp.message('We do not recognize your phone number, please visit %s to sign up.' % (Site.load().title, ))

    return HttpResponse(resp)