from django.db import models

class Site(models.Model):
    class Meta:
        verbose_name_plural = 'Site'

    title = models.CharField(max_length=100)
    contact_phone_number = models.CharField(max_length=100)
    contact_email = models.EmailField(max_length=100)

    def __str__(self):
        return self.title

    @classmethod
    def load(cls):
        site, _ = cls.objects.get_or_create(pk=1)
        return site

    def save(self, *args, **kwargs):
        self.id = 1

        return super().save(*args, **kwargs)