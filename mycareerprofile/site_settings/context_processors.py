from django.conf import settings
from site_settings.models import Site

def site(request):
    return {
        'site': Site.load()
    }
