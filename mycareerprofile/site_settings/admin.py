from django.contrib import admin
from .models import Site

@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'contact_phone_number', 
        'contact_email', 
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, *args, **kwargs):
        return False