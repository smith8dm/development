"""
WSGI config for mycareerprofile project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
import sys

# TODO: Pass this in via apache configuration.
sys.path.append('/mycareerprofile')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mycareerprofile.settings")

application = get_wsgi_application()
