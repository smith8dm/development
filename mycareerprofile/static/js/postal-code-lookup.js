var convertGoogleResponseToAddress = function(result) {
    var address = {};

    for(var i in result.address_components) {
        for(var j in result.address_components[i].types) {
            var addressField = '';

            switch(result.address_components[i].types[j]) {
                case 'neighborhood':
                    addressField = 'county';
                break;
                case 'locality':
                    // Only set, if this has not been set by the neighborhood.
                    if(!address.county) {
                        addressField = 'county';
                    }
                break;
                case 'administrative_area_level_1':
                    addressField = 'state';
                break;
                case 'country':
                    addressField = 'country';
                break;
            }

            if(addressField) {
                address[addressField] = result.address_components[i].long_name;
            }
        }
    }

    return address;
}

var postalCodeTimeout = null;

$('form [name="postal_code"]').keyup(function() {
    clearTimeout(postalCodeTimeout);

    var postalCode = $(this).val();

    var $form = $(this).parents('form');
    var $this = $(this).addClass('loading');
    var $countyInput = $('input[name="county"]', $form).prop('disabled', true);
    var $stateSelect = $('select[name="state"]', $form).prop('disabled', true);

    postalCodeTimeout = setTimeout(function() {
        $.get('https://maps.googleapis.com/maps/api/geocode/json', {
            key: 'AIzaSyDHXDXc5LeuDUE9KcBf3l1tEaZ9bCrYXjg',
            address: postalCode
        }, function(response) {
            $this.removeClass('loading');

            $countyInput.prop('disabled', false);
            $stateSelect.prop('disabled', false);

            if(response.status != 'OK') {
                return;
            }

            var address = convertGoogleResponseToAddress(response.results[0]);
            if(address.country != 'United States') {
                return;
            }

            $countyInput.val(address.county);
            $('option[value="' + address.state + '"]', $stateSelect).prop('selected', true);
        });
    }, 1000);
});