from collections import OrderedDict
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.db import connection
from django.db import models
from django.db.models.signals import post_delete
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from django.dispatch import receiver
from io import BytesIO
from pdf2image import convert_from_bytes
from site_settings.models import Site
from twilio.rest import Client

import datetime
import decimal
import logging
import math
import os
import random
import requests
import string
import uuid

PROFILE_TYPE_CHOICES = [
    ('candidate', 'I am looking to create an employee profile', ),
    ('prospect', 'I am looking for employees', ),
]

RESOURCE_TYPE_CHOICES = [
    ('conversation_video', 'Video Conversation', ),
    ('certification_photo', 'License/Certification', ),
    ('education_photo', 'Education', ),
    ('letter_of_recommendation_photo', 'Letter Of Recommendation', ),
    ('career_photo', 'Career Photo', ),
    ('work_video', 'Project/Work Video', ),
    ('profile_photo', 'Profile Photo', ),
    ('resume_photo', 'Resume Photo'),
    ('skill_assessment_photo', 'Skill Assessment Photo'),
    ('tool_photo', 'Tool Photo', ),
]

STATE_CHOICES = [
    ('Alabama', 'Alabama',),
    ('Alaska', 'Alaska',),
    ('Arizona', 'Arizona',),
    ('Arkansas', 'Arkansas',),
    ('California', 'California',),
    ('Colorado', 'Colorado',),
    ('Connecticut', 'Connecticut',),
    ('Delaware', 'Delaware',),
    ('District of Columbia', 'District of Columbia',),
    ('Florida', 'Florida',),
    ('Georgia', 'Georgia',),
    ('Hawaii', 'Hawaii',),
    ('Idaho', 'Idaho',),
    ('Illinois', 'Illinois',),
    ('Indiana', 'Indiana',),
    ('Iowa', 'Iowa',),
    ('Kansas', 'Kansas',),
    ('Kentucky', 'Kentucky',),
    ('Louisiana', 'Louisiana',),
    ('Maine', 'Maine',),
    ('Maryland', 'Maryland',),
    ('Massachusetts', 'Massachusetts',),
    ('Michigan', 'Michigan',),
    ('Minnesota', 'Minnesota',),
    ('Mississippi', 'Mississippi',),
    ('Missouri', 'Missouri',),
    ('Montana', 'Montana',),
    ('Nebraska', 'Nebraska',),
    ('Nevada', 'Nevada',),
    ('New Hampshire', 'New Hampshire',),
    ('New Jersey', 'New Jersey',),
    ('New Mexico', 'New Mexico',),
    ('New York', 'New York',),
    ('North Carolina', 'North Carolina',),
    ('North Dakota', 'North Dakota',),
    ('Ohio', 'Ohio',),
    ('Oklahoma', 'Oklahoma',),
    ('Oregon', 'Oregon',),
    ('Pennsylvania', 'Pennsylvania',),
    ('Rhode Island', 'Rhode Island',),
    ('South Carolina', 'South Carolina',),
    ('South Dakota', 'South Dakota',),
    ('Tennessee', 'Tennessee',),
    ('Texas', 'Texas',),
    ('Utah', 'Utah',),
    ('Vermont', 'Vermont',),
    ('Virginia', 'Virginia',),
    ('Washington', 'Washington',),
    ('West', 'West',),
    ('Wisconsin', 'Wisconsin',),
    ('Wyoming', 'Wyoming',),
]

YEARS_OF_EXPERIENCE_CHOICES = [
    (0, 'Less than 1 year of experience'),
    (1, "1 year of experience", ),
    (2, "2 years of experience", ),
    (3, "3 years of experience", ),
    (4, "4 years of experience", ),
    (5, "5 years of experience", ),
    (6, "6 years of experience", ),
    (7, "7 years of experience", ),
    (8, "8 years of experience", ),
    (9, "9 years of experience", ),
    (10, "10 years of experience", ),
    (11, "11 years of experience", ),
    (12, "12 years of experience", ),
    (13, "13 years of experience", ),
    (14, "14 years of experience", ),
    (15, "15 years of experience", ),
    (16, "16 years of experience", ),
    (17, "17 years of experience", ),
    (18, "18 years of experience", ),
    (19, "19 years of experience", ),
    (20, "20 years of experience", ),
    (99, "More than 20 years of experience", ),

]

BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))


class Career(models.Model):

    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    @classmethod
    def get_tree(cls):
        skills = Skill.objects.all()

        careers = {}

        for skill in skills:
            career = skill.career
            profession = skill.profession

            if career not in careers:
                careers[career] = {}

            if profession not in careers[career]:
                careers[career][profession] = []

            careers[career][profession].append(skill)

        return careers

class Profession(models.Model):

    class Meta:
        ordering = ['career__name', 'name']

    career = models.ForeignKey(Career, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Skill(models.Model):

    class Meta:
        ordering = ['career__name', 'profession__name', 'name']

    career = models.ForeignKey(Career, on_delete=models.CASCADE)
    profession = models.ForeignKey(Profession, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return '%s > %s > %s' % (self.career.name, self.profession.name, self.name, )

    @classmethod
    def get_by_id(cls, skill_id):
        cache_key = 'skill-%d' % (skill_id, )

        skill = cache.get(cache_key)
        if not skill:
            skill = cls.objects.get(id=skill_id)
            # Skills change infrequently, cache for 30 days.
            cache.set(cache_key, skill, 60 * 60 * 24 * 30)

        return skill


def resource_upload_path(instance, filename):
    filename, file_extension = os.path.splitext(filename)

    return 'static/profile/%d/resources/%s/%s-%s%s' % (
        instance.profile_id,
        instance.resource_type,
        filename,
        ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10)),
        file_extension
    )


class Profile(models.Model):
    share_id = models.CharField(max_length=50, unique=True, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    email_verified = models.BooleanField(default=False)
    cell_phone_number = models.CharField(max_length=50)
    cell_phone_number_e164 = models.CharField(max_length=50)
    cell_phone_number_verified = models.BooleanField(default=False)
    secondary_phone_number = models.CharField(blank=True, max_length=50)
    bio = models.TextField()
    profile_type = models.CharField(max_length=20, default=PROFILE_TYPE_CHOICES[0][0], choices=PROFILE_TYPE_CHOICES)
    is_available_for_work = models.BooleanField(default=True, choices=BOOL_CHOICES)
    is_reviewed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True, choices=BOOL_CHOICES)
    is_us_citizen = models.NullBooleanField()
    is_authorized_to_work_in_us = models.NullBooleanField()
    is_18_or_older = models.NullBooleanField()
    is_willing_to_relocate_for_work = models.BooleanField(default=False, choices=BOOL_CHOICES)
    finished_profile_setup = models.BooleanField(default=False, choices=BOOL_CHOICES)
    accepts_terms_of_service = models.NullBooleanField()
    county = models.CharField(max_length=100)
    state = models.CharField(max_length=100, choices=STATE_CHOICES)
    postal_code = models.CharField(max_length=32)
    distance_willing_to_travel = models.IntegerField(null=True, choices=[
        (miles, '{} miles'.format(miles), ) for miles in range(5, 51, 5)
    ])

    @classmethod
    def generate_share_id(cls, profile):
        return '%s-%06d' % (
            profile.first_name.lower(),
            profile.id,
        )

    def __str__(self):
        return '%s %s\'s Profile' % (self.first_name, self.last_name, )

    @property
    def skill_ids(self):
        return self.profileskill_set.values_list('skill__id', flat=True)

    def get_career_tree(self):
        profile_skills = self.profileskill_set.select_related().all()

        careers = {}

        for profile_skill in profile_skills:
            career = profile_skill.skill.career
            profession = profile_skill.skill.profession

            if career not in careers:
                careers[career] = {}

            if profession not in careers[career]:
                careers[career][profession] = []

            careers[career][profession].append(profile_skill)

        return careers

    def trigger_email_verification(self, email):
        return self.profileverification_set.create(
            target_field='email_verified',
            send_to=email)

    def trigger_cell_phone_number_verification(self, cell_phone_number):
        return self.profileverification_set.create(
            target_field='cell_phone_number_verified',
            send_to=cell_phone_number)

    def trigger_password_reset(self):
        return self.profilepasswordreset_set.create()


@receiver(post_save, sender=Profile)
def notify_admins_of_new_prospect(sender, instance, created, **kwargs):
    if not created:
        return

    if instance.profile_type == 'prospect':
        logging.debug('A new prospect has signed up, notifying admins.')

        for phone_number in settings.SMS_NUMBERS_ON_NEW_PROSPECT:
            client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

            message = client.messages.create(
                to=phone_number,
                from_=settings.TWILIO_SMS_VERIFICATION_NUMBER,
                body='A new prospect has signed up.\n{first_name} {last_name}\n{email}'.format(
                    first_name=instance.first_name,
                    last_name=instance.last_name,
                    email=instance.email
                )
            )

            logging.debug('Got message: %s' % (message, ))

class ProfilePasswordReset(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    token = models.CharField(max_length=200)
    date_expires = models.DateTimeField(auto_now=False, auto_now_add=False)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = uuid.uuid4().hex
            self.date_expires = datetime.datetime.now() + datetime.timedelta(hours=1)

        return super().save(*args, **kwargs)


@receiver(pre_save, sender=ProfilePasswordReset)
def send_password_reset_email(sender, instance, **kwargs):
    logging.debug('Checking to see if we should send a password reset email')

    if instance.id:
        logging.debug('This password reset is not new')
        return

    logging.debug('About to send email verification')

    reset_password_url = settings.RESET_PASSWORD_URL.format(token=instance.token)

    response = requests.post('https://api.mailgun.net/v3/%s/messages' % (settings.MAILGUN_DOMAIN, ), {
        'from': settings.MAILGUN_FROM_EMAIL,
        'to': instance.profile.email,
        'subject': 'Please Reset Your Password',
        'text': 'Please visit {reset_password_url} to reset your password.'.format(reset_password_url=reset_password_url),
        'html': 'Please visit <a href="{reset_password_url}">{reset_password_url}</a> to reset your password.'.format(reset_password_url=reset_password_url)
    }, auth=('api', settings.MAILGUN_API_KEY))

    logging.debug('Sending reset password email')
    logging.debug('Please visit {reset_password_url} to reset your password.'.format(reset_password_url=reset_password_url))

    logging.debug('Got response: %s' % (response.content, ))


@receiver(pre_save, sender=ProfilePasswordReset)
def send_password_reset_sms(sender, instance, **kwargs):
    logging.debug('Checking to see if we should send a password reset SMS')

    if instance.id:
        logging.debug('This password reset is not new')
        return

    if not instance.profile.cell_phone_number_e164:
        logging.debug('User does not have a cell phone number on file.')
        return

    logging.debug('About to send SMS verification')

    reset_password_url = settings.RESET_PASSWORD_URL.format(token=instance.token)

    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

    message = client.messages.create(
        to=instance.profile.cell_phone_number_e164,
        from_=settings.TWILIO_SMS_VERIFICATION_NUMBER,
        body='Please visit {reset_password_url} to reset your password.'.format(reset_password_url=reset_password_url)
    )

    logging.debug('Got message: %s' % (message, ))


@receiver(pre_save, sender=Profile)
def lookup_postal_code(sender, instance, **kwargs):
    if instance.postal_code:
        PostalCode.from_postal_code(instance.postal_code)


@receiver(post_save, sender=Profile)
def set_share_id(sender, instance, **kwargs):
    share_id = sender.generate_share_id(instance)

    if instance.share_id != share_id:
        instance.share_id = share_id
        instance.save()

class ProfileSkill(models.Model):

    class Meta:
        ordering = ['skill__career__name', 'skill__profession__name', 'skill__name']

    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    years_of_experience = models.PositiveSmallIntegerField(default=0, choices=YEARS_OF_EXPERIENCE_CHOICES)

    @property
    def name_and_experience(self):
        skill = Skill.get_by_id(self.skill_id)

        if self.years_of_experience > 0:
            return '%s with %d year%s of experience' % (
                skill.name,
                self.years_of_experience,
                's' if self.years_of_experience != 1 else ''
            )

        return skill.name


class ProfileLocation(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    county = models.CharField(blank=True, max_length=100)
    state = models.CharField(blank=True, max_length=100, choices=STATE_CHOICES)
    postal_code = models.CharField(blank=True, max_length=32)
    location_type = models.CharField(max_length=100, choices=[
        ('residence', 'Residence', ),
        ('willing_to_work', 'Willing To Work', ),
    ], default='residence')

    def __str__(self):
        return '%s, %s' % (self.county, self.state, )


class Testimonial(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    testimonial = models.TextField()
    source = models.CharField(max_length=100)

    def __str__(self):
        return 'Testimonial for %s from %s' % (
            self.profile.first_name, self.source,
        )

class ResourceUploadManager(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    resource_type = models.CharField(max_length=50, choices=RESOURCE_TYPE_CHOICES)
    file = models.FileField(max_length=500, upload_to=resource_upload_path)

    def __str__(self):
        return '%s %s' % (self.get_resource_type_display(), self.file.name, )

    def upload_resources(self):
        self.resource_set.all().delete()

        resources = []

        filename, file_extension = os.path.splitext(self.file.name)
        filename = os.path.basename(filename)

        if file_extension == '.doc' or file_extension == '.docx':
            url = 'https://v2.convertapi.com/%s/to/pdf' % (file_extension.replace('.', ''), )
            params = {'secret': settings.CONVERTAPI_SECRET}
            files = {'file': self.file}
            headers = {'Accept': 'application/octet-stream'}

            response = requests.post(url, params=params, files=files, headers=headers)

            if response.status_code != 200:
                raise Exception("Failed to upload your word doc.")

            pdf_resource_upload_manager = ResourceUploadManager(
                profile=self.profile,
                resource_type=self.resource_type
            )
            pdf_resource_upload_manager.file.save('%s.pdf' % (filename, ), ContentFile(response.content), save=False)
            pdf_resource_upload_manager.save()

            return pdf_resource_upload_manager.upload_resources()

        elif file_extension == '.pdf':
            jpeg_images = convert_from_bytes(self.file.read(), fmt='jpeg')

            for i in range(len(jpeg_images)):
                jpeg_io = BytesIO()
                jpeg_images[i].save(jpeg_io, jpeg_images[i].format)

                resource = Resource(
                    profile=self.profile,
                    resource_upload_manager=self,
                    resource_type=self.resource_type
                )
                resource.original_file.save('%s-%d.jpeg' % (filename, i + 1), ContentFile(jpeg_io.getvalue()), save=False)
                resource.file = resource.original_file
                resource.save()

                resources.append(resource)
        else:
            resources.append(Resource.objects.create(
                profile=self.profile,
                resource_upload_manager=self,
                resource_type=self.resource_type,
                original_file=self.file,
                file=self.file
            ))

        return resources


class Resource(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    resource_type = models.CharField(max_length=50, choices=RESOURCE_TYPE_CHOICES)
    original_file = models.FileField(max_length=500, upload_to=resource_upload_path, blank=True, null=True)
    file = models.FileField(max_length=500, upload_to=resource_upload_path)
    sort_order = models.IntegerField(default=1, help_text="Sorted in ascending order by resource type.")
    resource_upload_manager = models.ForeignKey(ResourceUploadManager, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.get_resource_type_display()


class PostalCode(models.Model):
    postal_code = models.CharField(blank=True, max_length=32, unique=True)
    latitude = models.DecimalField(max_digits=12, decimal_places=9)
    cos_latitude = models.DecimalField(max_digits=12, decimal_places=9)
    sin_latitude = models.DecimalField(max_digits=12, decimal_places=9)
    longitude = models.DecimalField(max_digits=12, decimal_places=9)
    cos_longitude = models.DecimalField(max_digits=12, decimal_places=9)
    sin_longitude = models.DecimalField(max_digits=12, decimal_places=9)

    @classmethod
    def find_within_miles(cls, postal_code, miles):
        postal_code_obj = cls.from_postal_code(postal_code)

        earth_radius_in_miles = 3959.0

        allowed_distance = math.cos(float(miles) / earth_radius_in_miles)

        postal_code_with_distances = []

        with connection.cursor() as cursor:
            cursor.execute('''SELECT pc.postal_code, (
                    %s * sin_latitude + %s * cos_latitude *
                    (cos_longitude * %s + sin_longitude * %s)
                ) distance
                FROM profiles_postalcode pc
                GROUP BY pc.postal_code
                HAVING distance >= %s
                ORDER BY distance DESC''', (
                postal_code_obj.sin_latitude,
                postal_code_obj.cos_latitude,
                postal_code_obj.cos_longitude,
                postal_code_obj.sin_longitude,
                allowed_distance
            ))

            for postal_code, distance in cursor.fetchall():
                distance_in_miles = math.acos(min(distance, 1)) * earth_radius_in_miles

                postal_code_with_distances.append((postal_code, distance_in_miles, ))

        return postal_code_with_distances

    @classmethod
    def from_postal_code(cls, postal_code):
        try:
            return cls.objects.get(postal_code=postal_code)
        except cls.DoesNotExist:
            response = requests.get('https://maps.googleapis.com/maps/api/geocode/json', params={
                'key': 'AIzaSyDHXDXc5LeuDUE9KcBf3l1tEaZ9bCrYXjg',
                'address': postal_code
            }).json()

            try:
                result = response['results'][0]
            except IndexError:
                raise PostalCode.DoesNotExist('Postal code is not valid.')

            latitude = result['geometry']['location']['lat']
            longitude = result['geometry']['location']['lng']

            return PostalCode.objects.create(
                postal_code=postal_code,
                latitude=latitude,
                cos_latitude=math.cos(math.radians(latitude)),
                sin_latitude=math.sin(math.radians(latitude)),
                longitude=longitude,
                cos_longitude=math.cos(math.radians(longitude)),
                sin_longitude=math.sin(math.radians(longitude)),
            )

class ProfileVerification(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    target_field = models.CharField(max_length=30, choices=[
        ('email_verified', 'Email', ),
        ('cell_phone_number_verified', 'Cell Phone Number', )])
    send_to = models.CharField(max_length=200)
    token = models.CharField(max_length=200)
    is_verified = models.BooleanField(default=False)
    date_verified = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    date_expires = models.DateTimeField(auto_now=False, auto_now_add=False)

    def set_profile_verification_status_to(self, value):
        setattr(self.profile, self.target_field, value)
        self.profile.save(update_fields=[self.target_field])

    def mark_verified(self):
        self.is_verified = True
        self.date_verified = datetime.datetime.now()
        self.save()

        self.set_profile_verification_status_to(True)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = uuid.uuid4().hex
            self.date_expires = datetime.datetime.now() + datetime.timedelta(hours=1)

        return super().save(*args, **kwargs)


@receiver(pre_save, sender=ProfileVerification)
def send_email_verification(sender, instance, **kwargs):
    logging.debug('Checking to see if we should send email verification')

    if instance.id:
        logging.debug('This verifier is not new')
        return

    if instance.target_field != 'email_verified':
        logging.debug('This verifier is not for emails')
        return

    logging.debug('About to send email verification')

    instance.set_profile_verification_status_to(False)

    verification_url = settings.MAILGUN_VERIFICATION_URL.format(token=instance.token)

    response = requests.post('https://api.mailgun.net/v3/%s/messages' % (settings.MAILGUN_DOMAIN, ), {
        'from': settings.MAILGUN_FROM_EMAIL,
        'to': instance.send_to,
        'subject': 'Please Verify Your Email',
        'text': 'Please visit {verification_url} to verify your account.'.format(verification_url=verification_url),
        'html': 'Please visit <a href="{verification_url}">{verification_url}</a> to verify your account.'.format(verification_url=verification_url)
    }, auth=('api', settings.MAILGUN_API_KEY))

    logging.debug('Got response: %s' % (response.content, ))



@receiver(pre_save, sender=ProfileVerification)
def send_cell_phone_number_verification(sender, instance, **kwargs):
    logging.debug('Checking to see if we should send cell phone verification')

    if instance.id:
        logging.debug('This verifier is not new')
        return

    if instance.target_field != 'cell_phone_number_verified':
        logging.debug('This verifier is not for cell phones')
        return

    logging.debug('About to send cell phone verification')

    instance.set_profile_verification_status_to(False)

    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

    message = client.messages.create(
        to=instance.send_to,
        from_=settings.TWILIO_SMS_VERIFICATION_NUMBER,
        body='Please reply "Yes" to verify your phone number for %s.' % (Site.load().title, )
    )

    logging.debug('Got message: %s' % (message, ))

    instance.send_to = message.to
