from .models import PostalCode
from .models import Profession
from .models import Profile
from .models import ProfileSkill
from .models import Skill
from django import forms
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from twilio.rest import Client

import twilio.rest


class CellPhoneNumberValidatorMixin(object):
    def clean_cell_phone_number(self):
        cell_phone_number = self.cleaned_data['cell_phone_number']

        if not cell_phone_number:
            return cell_phone_number

        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

        try:
            number = client.lookups.phone_numbers(self.cleaned_data['cell_phone_number']).fetch()
        except twilio.rest.TwilioException:
            raise forms.ValidationError("Invalid phone number provided.")

        return number.national_format


class ProfileSearchForm(forms.Form):
    first_name = forms.CharField(label='First Name', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'First Name'
    }))
    professions = forms.ModelMultipleChoiceField(queryset=Profession.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)
    skills = forms.ModelMultipleChoiceField(queryset=Skill.objects.all(),
        widget=forms.CheckboxSelectMultiple, required=False)
    jobsite_postal_code = forms.CharField(label='Jobsite Postal Code', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jobsite Postal Code'
    }))
    share_id = forms.CharField(label='Share ID', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Share ID'
    }))

    def clean_jobsite_postal_code(self):
        jobsite_postal_code = self.cleaned_data['jobsite_postal_code']

        if not jobsite_postal_code:
            return jobsite_postal_code

        try:
            PostalCode.from_postal_code(jobsite_postal_code)
        except PostalCode.DoesNotExist as e:
            raise forms.ValidationError('%s' % (e, ))

        return jobsite_postal_code


class UserSignupForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'password',
            'email',
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'First Name',
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Last Name',
            }),
            'username': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Username',
            }),
            'password': forms.PasswordInput(attrs={
                'class': 'form-control',
                'placeholder': 'Password',
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email',
            }),
        }

    password_confirm = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Confirm Password',
    }))

    def clean(self):
        cleaned_data = super().clean()

        if User.objects.filter(username=cleaned_data.get('username')).count():
            self.add_error('username', 'Username already taken.')

        if cleaned_data.get('password') != cleaned_data.get('password_confirm'):
            self.add_error('password_confirm', 'Passwords do not match.')

        if 'password' in cleaned_data:
            cleaned_data['password'] = make_password(cleaned_data['password'])

        return cleaned_data


class ProfileForgotPasswordForm(forms.Form, CellPhoneNumberValidatorMixin):
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email'
    }))
    cell_phone_number = forms.CharField(label='Cell Phone Number', required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cell Phone Number'
    }))

    def clean(self):
        self.profiles = []

        cleaned_data = super().clean()

        if not cleaned_data.get('email') and not cleaned_data.get('cell_phone_number'):
            raise forms.ValidationError("Please provide an email or a cell phone number.")

        if cleaned_data.get('email'):
            self.profiles.extend([p for p in Profile.objects.filter(email=cleaned_data['email'])])

        if cleaned_data.get('cell_phone_number'):
            self.profiles.extend([p for p in Profile.objects.filter(cell_phone_number=cleaned_data['cell_phone_number'])])

        return cleaned_data

class ProfilePasswordResetForm(forms.Form):
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Password'
    }))
    password_confirm = forms.CharField(label='Password (Confirm)', required=True, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Password (Confirm)'
    }))

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('password') != cleaned_data.get('password_confirm'):
            raise forms.ValidationError({
                'password': ['The passwords must match.']
            })

        return cleaned_data


class ProfileSignupForm(forms.ModelForm, CellPhoneNumberValidatorMixin):
    class Meta:
        model = Profile
        fields = [
            'profile_type',
            'cell_phone_number',
            'is_us_citizen',
            'is_authorized_to_work_in_us',
            'is_18_or_older',
            'accepts_terms_of_service',

            # These fields are only set so they can be set by the values
            # from the user form.
            'first_name',
            'last_name',
            'email',
        ]
        labels = {
            'is_us_citizen': 'Are you a citizen of the United States?',
            'is_authorized_to_work_in_us': 'Are you permitted to work in the United States?',
            'is_18_or_older': 'Are you at least 18 years of age?'
        }
        widgets = {
            'profile_type': forms.HiddenInput(),
            'cell_phone_number': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Cell Phone Number',
            }),
            'is_us_citizen': forms.CheckboxInput(attrs={
                'class': 'form-checkbox',
            }),
            'is_authorized_to_work_in_us': forms.CheckboxInput(attrs={
                'class': 'form-checkbox',
            }),
            'is_18_or_older': forms.CheckboxInput(attrs={
                'class': 'form-checkbox',
            }),
            'accepts_terms_of_service': forms.CheckboxInput(attrs={
                'class': 'form-checkbox',
            }),
        }

    def clean_is_18_or_older(self):
        is_18_or_older = self.cleaned_data['is_18_or_older']

        if not is_18_or_older:
            raise forms.ValidationError("You must be 18 years of age or older to sign up.")

        return is_18_or_older

    def clean_accepts_terms_of_service(self):
        accepts_terms_of_service = self.cleaned_data['accepts_terms_of_service']

        if not accepts_terms_of_service:
            raise forms.ValidationError("You accept our terms of service to sign up.")

        return accepts_terms_of_service

    def clean(self):
        cleaned_data = super().clean()

        if not (cleaned_data['is_us_citizen'] or cleaned_data['is_authorized_to_work_in_us']):
            raise forms.ValidationError({
                'is_authorized_to_work_in_us': ['You must be a US citizen or authorized to work in the US to sign up.']
            })

        return cleaned_data

    def save(self, *args, **kwargs):
        profile = super().save(*args, **kwargs)

        profile.trigger_email_verification(self.cleaned_data['email'])
        profile.trigger_cell_phone_number_verification(self.cleaned_data['cell_phone_number'])

        return profile


class ProfileAboutMeForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'bio',
        ]
        labels = {
            'bio': 'Tell Us About Yourself',
        }
        widgets = {
            'bio': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Tell us more about yourself...',
                'rows': 8
            }),
        }

class ProfilePersonalInfoForm(forms.ModelForm, CellPhoneNumberValidatorMixin):
    class Meta:
        model = Profile
        fields = [
            'first_name',
            'last_name',
            'email',
            'cell_phone_number',
            'secondary_phone_number',
            'county',
            'state',
            'postal_code',
        ]
        labels = {
            'cell_phone_number': 'Cell Phone Number',
            'secondary_phone_number': 'Secondary Phone Number',
            'county': 'City/Town',
            'postal_code': 'Postal Code',
        }
        widgets = {
            'first_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'First Name',
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Last Name',
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email',
            }),
            'cell_phone_number': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Cell Phone Number',
            }),
            'secondary_phone_number': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Secondary Phone Number',
            }),
            'county': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'City/Town',
            }),
            'state': forms.Select(attrs={
                'class': 'form-control',
            }),
            'postal_code': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Postal Code',
            }),
        }

    def clean_postal_code(self):
        postal_code = self.cleaned_data['postal_code']

        try:
            PostalCode.from_postal_code(postal_code)
        except PostalCode.DoesNotExist as e:
            raise forms.ValidationError('%s' % (e, ))

        return postal_code

    def save(self, *args, **kwargs):
        self.instance.is_reviewed = False

        profile = super().save(*args, **kwargs)

        if 'email' in self.changed_data:
            self.instance.trigger_email_verification(self.cleaned_data['email'])

        if 'cell_phone_number' in self.changed_data:
            self.instance.trigger_cell_phone_number_verification(self.cleaned_data['cell_phone_number'])

        return profile


class ProfileWorkStatusForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'distance_willing_to_travel',
            'is_available_for_work',
            'is_active',
            'is_willing_to_relocate_for_work'
        ]
        labels = {
            'distance_willing_to_travel': 'Distance you\'re willing to travel from your home location.'
        }
        widgets = {
            'distance_willing_to_travel': forms.Select(attrs={
                'class': 'form-control',
            }),
            'is_available_for_work':  forms.RadioSelect(attrs={
                'class': 'radio-inline',
            }),
            'is_active':  forms.RadioSelect(attrs={
                'class': 'radio-inline',
            }),
            'is_willing_to_relocate_for_work': forms.RadioSelect(attrs={
                'class': 'radio-inline',
            }),
        }


class ProfileSkillForm(forms.ModelForm):
    class Meta:
        model = ProfileSkill
        fields = [
            'skill',
            'years_of_experience'
        ]

    def save(self, *args, **kwargs):
        if self.instance.profile_id:
            self.instance.profile.is_reviewed = False
            self.instance.profile.save(update_fields=['is_reviewed'])

        return super().save(*args, **kwargs)
