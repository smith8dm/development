from django.conf import settings
from django.urls import resolve
from twilio.rest import Client


next_page_map = {
    'manage-profile-personal-info': 'manage-profile-about-me',
    'manage-profile-about-me': 'manage-profile-add-skills',
    'manage-profile-add-skills': 'manage-profile-skills',
    'manage-profile-skills': 'manage-profile-work-status',
    'manage-profile-work-status': 'manage-profile-profile-photos',
    'manage-profile-profile-photos': 'manage-profile-conversation-videos',
    'manage-profile-conversation-videos': 'manage-profile-certifications',
    'manage-profile-certifications': 'manage-profile-education',
    'manage-profile-education': 'manage-profile-tool-photos',
    'manage-profile-tool-photos': 'manage-profile-resume',
    'manage-profile-resume': 'manage-profile-career-photos',
    'manage-profile-career-photos': 'manage-profile-work-videos',
    'manage-profile-work-videos': 'manage-profile-skill-assessments',
    'manage-profile-skill-assessments': 'manage-profile-letters-of-recommendation',
    'manage-profile-letters-of-recommendation': 'manage-profile-complete'
}

previous_page_map = dict((v, k) for (k,v) in next_page_map.items())


def get_previous_manager_page(url_name):
    return previous_page_map[url_name]


def get_next_manager_page(url_name):
    return next_page_map[url_name]


def determine_subsequent_profile_manager_page_for_request(request):
    current_url_name = resolve(request.path_info).url_name

    if request.POST.get('go_back'):
        return get_previous_manager_page(current_url_name)

    return get_next_manager_page(current_url_name)