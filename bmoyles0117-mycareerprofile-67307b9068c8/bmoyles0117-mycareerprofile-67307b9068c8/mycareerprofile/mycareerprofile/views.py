from django.db import connection
from django.db.utils import OperationalError
from django.shortcuts import HttpResponse
from django.shortcuts import render

def healthz(request):
    try:
        with connection.cursor() as cursor:
            return HttpResponse('OK', status=200)

    except OperationalError as e:
        print('Database connection failed: %s' % (e, ))
        return HttpResponse('Database failure', status=500)

def home(request):
	return render(request, 'home.html')